// IDS344 - 2022-01 - Grupo 1 - Reina y dos torres
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597
//
//  24 de marzo del 2022

#include <iostream>
#include <string>

using namespace std;

struct Coordinates {
    int Down;
    int Right;
};

void FillDeckWithEmptyCell(string deck[8][8]) {

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            deck [i][j] = " ";
        }
    }
};

void InsertToDeck(string deck[8][8], Coordinates cord, string toPlace) {

    deck[7 - cord.Down][cord.Right] = toPlace; //7 - row porque para matriz es desde arriba hacia abajo, para chess es al reves
    
};

void ShowLine(string last, bool hasPlus)
{
    for (int i = 0; i < 8; i++)
    {
        if (i == 0)
            cout << "  " << last;
        else
        {
            if (hasPlus)
                cout << "+";
            else
                cout << "-";
        }
            

        for (int j = 0; j < 3; j++)
        {
            cout << "-";
        }
    }
    
    cout << last;

    cout << endl;
}

void ShowDeck(string deck[8][8])
{
    for (int i = 0; i < 8; i++)
    {   
        if(i == 0)
            ShowLine(".", false);
        else
            ShowLine("|", true);

        cout << 8 - i << " "; // numeros

        cout << "| ";

        for (int j = 0; j < 8; j++)
        {
            cout << deck[i][j] + " | ";
        }

        cout << endl;
    }

    ShowLine("*", false);

    cout << "    A   B   C   D   E   F   G   H";
}



int ConvertLetterToNum(char letter)
{
    int num = 0;

    switch (letter) {
    case 'A':
        num = 0;
        break;
    
    case 'B':
        num = 1;
        break;

    case 'C':
        num = 2;
        break;
    
    case 'D':
        num = 3;
        break;
    
    case 'E':
        num = 4;
        break;
    
    case 'F':
        num = 5;
        break;
    
    case 'G':
        num = 6;
        break;
    
    case 'H':
        num = 7;
        break;
    

    case 'a':
        num = 0;
        break;

    case 'b':
        num = 1;
        break;

    case 'c':
        num = 2;
        break;

    case 'd':
        num = 3;
        break;

    case 'e':
        num = 4;
        break;

    case 'f':
        num = 5;
        break;

    case 'g':
        num = 6;
        break;

    case 'h':
        num = 7;
        break;

    default:
        num = 10;
}

    return num;
}

void InsertFigure(string deck[8][8], string figureToInsert)
{
    Coordinates cordinates = { -1, -1};

    string position, fullName;

    if (figureToInsert == "Q")
        fullName = "Queen";
    else if (figureToInsert == "T")
        fullName = "Torre";

    do
    {
        cout << "\n\nEnter the position of " << fullName << " (Ej: A1, B8) :\n";
        cin >> position;

        if (position.length() == 1)//Si era introducido el valor �nico(en este caso) , repetir el bucle
            position = "Q9"; //Esta posicion no existe

        cordinates.Down = int(position[1] - 49); // -48 para convertir char a int y -1 mas porque arreglo se empiwza desde 0;
        cordinates.Right = ConvertLetterToNum(position[0]);
       
        if (deck[7 - cordinates.Down][cordinates.Right] != " ")
        {
            cordinates.Down = -1;
            cout << "\nYou cant place figure here!";
        }

    } while ((cordinates.Down < 0 || cordinates.Down > 7) || (cordinates.Right < 0 || cordinates.Right > 7));

    InsertToDeck(deck, cordinates, figureToInsert);
}

Coordinates SearchFigure(string deck[8][8], string figureToFind, bool isFirstFigureToFind)
{
    Coordinates InitialLoc = { 0,0 }; //0 elemento es renglon y 1 columna

    for (int i = 0; i < 8; i++) //encontrar donde est� Queen
    {
        for (int j = 0; j < 8; j++)
        {
            if (deck[i][j] == figureToFind)
            {
                if (isFirstFigureToFind) //si nececitamos primera, salimos aqui
                {
                    InitialLoc.Down = i;
                    InitialLoc.Right = j;

                    j = 9;
                    i = 9;//Para salir del loop
                }
                else
                    isFirstFigureToFind = true; //Else cuando veremos la segunda figura de este tipo
            }
        }
    }

    return InitialLoc;
}

void CheckDirection(string deck[8][8], Coordinates initialLocQueen, int movementDown, int MovementRight, Coordinates firstTorre, Coordinates secondTorre)
{
    int row = initialLocQueen.Down;
    int column = initialLocQueen.Right;

    Coordinates initialLocUpdated = {0,0};

    if (row + movementDown < 0 || row + movementDown >7 ||
        column + MovementRight < 0 || column + MovementRight >7) //Si El siguiente cell to step esta fuera de la tabla, terminar recursi�n
        return;
    
    row = row + movementDown;
    column = column + MovementRight;

    if (deck[row][column] == "T")//Si se encuentra con el T en la celda falta el permiso para eliminar a ella,
    {
        if(firstTorre.Down == secondTorre.Down || firstTorre.Right == secondTorre.Right)
            deck[row][column] = "T";//En este caso, al eliminar torre, reina estar� en peligro
        else
            deck[row][column] = "K";

        return;
    }

    else if(row == firstTorre.Down || row == secondTorre.Down) //First iteration with no consideration of beating
        deck[row][column] = "x";

    else if (column == firstTorre.Right || column == secondTorre.Right)
        deck[row][column] = "x";

    else
        deck[row][column] = "v";

    initialLocUpdated.Down = row;
    initialLocUpdated.Right = column;

    CheckDirection(deck, initialLocUpdated, movementDown, MovementRight, firstTorre, secondTorre);
}

void ShowTrayectories(string deck[8][8])
{
    Coordinates InitialLocQueen = SearchFigure( deck, "Q", true);

    Coordinates firstTorre = SearchFigure(deck, "T", true);
    Coordinates secondTorre = SearchFigure(deck, "T", false);

    CheckDirection(deck, InitialLocQueen, 1, 0, firstTorre, secondTorre); // abajo
    CheckDirection(deck, InitialLocQueen, -1, 0, firstTorre, secondTorre); // arriba
    CheckDirection(deck, InitialLocQueen, 0, 1, firstTorre, secondTorre); //derecha
    CheckDirection(deck, InitialLocQueen, 0, -1, firstTorre, secondTorre); //izquierda

    CheckDirection(deck, InitialLocQueen, 1, -1, firstTorre, secondTorre); //abajo izquierda
    CheckDirection(deck, InitialLocQueen, 1, 1, firstTorre, secondTorre); //abajo derecha
    CheckDirection(deck, InitialLocQueen, -1, -1, firstTorre, secondTorre); //arriba izquierda
    CheckDirection(deck, InitialLocQueen, -1, 1, firstTorre, secondTorre); // arriba derecha
}

void Pause()
{
    cout << "\n";
    system("pause");
}

void ClearConsole()
{
    system("cls");
    cout << "-- Torres enemigas --\n\n";
}

int main()
{
    while (true)
    {
        string deck[8][8];
        string position;

        ClearConsole();

        FillDeckWithEmptyCell(deck);

        ShowDeck(deck);

        InsertFigure(deck, "Q");

        ClearConsole();
        ShowDeck(deck);

        InsertFigure(deck, "T");

        ClearConsole();
        ShowDeck(deck);

        InsertFigure(deck, "T");

        ClearConsole();
        ShowDeck(deck);

        ShowTrayectories(deck);

        cout << "\n\nQueen has this options to make a turn:\n\n";

        ShowDeck(deck);

        cout << "\n\nx - Queen will be eliminated";
        cout << "\nv - Queen can go to this cell and not to be eliminated";
        cout << "\nK - Queen can eliminate other figure and not to be eliminated";

        Pause();
    }
}

